import { Routes, Route, BrowserRouter } from 'react-router-dom'
import { Home } from '../pages/Home'
import { File } from 'src/pages/File/File'
export function RoutesList() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/*' element={<Home />} />
				<Route path='/file/:path*' element={<File />} />
			</Routes>
		</BrowserRouter>
	)
}
