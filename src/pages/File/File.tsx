import { FC, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Container } from 'src/components/pages/file/container/Container'
import { ErrorPlug } from 'src/components/pages/file/error-plug/ErrorPlug'
import { Header } from 'src/components/pages/file/header/header'
import { LoadingSpin } from 'src/components/ui/loading-spin/LoadingSpin'
import { openNotificationWithIcon } from 'src/helpers/notifications'
import { parseRoutePath } from 'src/helpers/utils/parseRoutePath'
import { dropboxService } from 'src/services/dropbox.service'

interface Props {}
export const File: FC<Props> = props => {
	const [isLoading, setIsLoading] = useState(false)
	const [isError, setIsError] = useState(false)

	const routeParams = useParams()
	const path = parseRoutePath(routeParams)
	let [url, setUrl] = useState('')

	useEffect(() => {
		setIsLoading(true)
		dropboxService
			.getFileThumbNail(path)
			.then(data => setUrl(URL.createObjectURL(data)))
			.catch(err => setIsError(true))
			.finally(() => setIsLoading(false))
	}, [path])

	const onMutedButton = () =>
		openNotificationWithIcon(
			'warning',
			'Warning',
			'this functionality is still under development'
		)

	const onDownload = () => {
		if (!!path) {
			dropboxService.downloadFile(path)
		} else {
			openNotificationWithIcon('warning', 'Warning', 'unable to download')
		}
	}

	return (
		<>
			<Header
				onDownload={onDownload}
				onMutedButton={onMutedButton}
				path={path}
			/>
			<Container>
				{isLoading ? (
					<LoadingSpin />
				) : (
					<>
						{isError ? (
							<ErrorPlug />
						) : (
							<img
								alt='img'
								id='image'
								src={url}
								style={{
									position: 'absolute',
									maxHeight: '95%',
									maxWidth: '100%',
									overflow: 'auto'
								}}
							/>
						)}
					</>
				)}
			</Container>
		</>
	)
}
