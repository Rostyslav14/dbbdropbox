import { FC, FormEvent, useEffect, useState } from 'react'
import { Container } from '../components/pages/home/container/Container'
import { Toolbar } from '../components/pages/home/toolbar/Toolbar'
import { TableBar } from '../components/pages/home/tablebar/TableBar'
import { dropboxService } from 'src/services/dropbox.service'
import { SelectBasic } from 'src/components/ui/select-basic/SelectBasic'
import { IReadFolderEntry } from 'src/helpers/types/responseTypes'
import { useNavigate, useParams } from 'react-router-dom'
import { openNotificationWithIcon } from 'src/helpers/notifications'
import { parseRoutePath } from 'src/helpers/utils/parseRoutePath'
import { ModalCreateFolder } from 'src/components/modals/modal-create-folder/ModalCreateFolder'

interface Props {}
export const Home: FC<Props> = props => {
	const navigate = useNavigate()
	const [currentFiles, setCurrentFiles] = useState<IReadFolderEntry[]>([])
	const [isLoading, setIsLoading] = useState<boolean>(false)
	const [isCreateFolderOpen, setIsCreateFolderOpen] = useState<boolean>(false)

	const params = useParams()
	let currentPath = parseRoutePath(params)

	useEffect(() => {
		//on every url change fetch new folder entry
		setIsLoading(true)
		dropboxService
			.readFolder(currentPath)
			.then(res => setCurrentFiles(res))
			.finally(() => setIsLoading(false))
	}, [currentPath])

	const onDownloadRowItem = (row: IReadFolderEntry) => {
		//check file type
		switch (row['.tag']) {
			//if it is a folder download the .zip
			case 'folder':
				dropboxService.downloadFolder(row.path_lower!)
				break
			case 'file':
				//if it is a file -  download the file
				dropboxService.downloadFile(row.path_lower!)
				break
			default:
				openNotificationWithIcon('warning', 'Warning', 'unable to download')
		}
	}
	const onDeleteRowItem = (row: IReadFolderEntry) => {
		setIsLoading(true)
		dropboxService
			.deleteFile(row.path_lower!)
			.then(() => dropboxService.readFolder(currentPath))
			.then(data => setCurrentFiles(data))
			.finally(() => setIsLoading(false))
	}
	const onUploadFile = (file: File) => {
		setIsLoading(true)
		dropboxService
			.uploadFile(file, currentPath)
			.then(() => dropboxService.readFolder(currentPath))
			.then(data => setCurrentFiles(data))
			.finally(() => setIsLoading(false))
	}
	const onUploadFolder = (files: FileList) => {
		setIsLoading(true)
		dropboxService
			.uploadFolder(files, currentPath)
			.then(() => dropboxService.readFolder(currentPath))
			.then(data => setCurrentFiles(data))
			.finally(() => setIsLoading(false))
	}

	const onOpenItem = (row: IReadFolderEntry) => {
		if (row['.tag'] === 'folder') {
			navigate(row.path_lower!)
		} else {
			navigate('file' + row.path_lower!)
		}
	}

	const onToggleModal = () => {
		setIsCreateFolderOpen(prev => !prev)
	}
	const onCreateFolder = (name: string) => {
		dropboxService
			.createFolder(currentPath, name)
			.then(() => dropboxService.readFolder(currentPath))
			.then(data => setCurrentFiles(data))
		setIsCreateFolderOpen(false)
	}

	return (
		<main>
			<Container>
				<Toolbar>
					<ModalCreateFolder
						isOpen={isCreateFolderOpen}
						onCreateFolder={onCreateFolder}
						onToggleModal={onToggleModal}
					/>
					<SelectBasic
						onUploadFolder={onUploadFolder}
						onUpload={onUploadFile}
					/>
				</Toolbar>
				<TableBar
					onDeleteItem={onDeleteRowItem}
					onDownloadItem={onDownloadRowItem}
					loading={isLoading}
					onOpen={onOpenItem}
					data={currentFiles}
				/>
			</Container>
		</main>
	)
}
