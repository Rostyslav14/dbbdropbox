export type uploadedItemType =
	| 'deleted'
	| 'folder'
	| 'audio'
	| 'font'
	| 'model'
	| 'text'
	| 'video'
	| 'image'
