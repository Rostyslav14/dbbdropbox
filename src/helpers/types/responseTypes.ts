interface Metadata {
	'.tag': 'file' | 'folder' | 'deleted'
	name: string
	path_lower?: string
	path_display?: string
	parent_shared_folder_id?: string
	preview_url?: string
}

interface IFolder extends Metadata {
	'.tag': 'folder'
	id: number
	shared_folder_id?: string
	sharing_info?: string
}

export interface IFile extends Metadata {
	'.tag': 'file'
	id: string
	client_modified: string
	server_modified: string
	fileBlob?: {
		readonly size: number
		readonly type: string
		slice(start?: number, end?: number, contentType?: string): Blob
	}
	rev: string
	size: number
	media_info?: string
	symlink_info?: string
	sharing_info?: string
	is_downloadable?: boolean
	export_info?: string
	has_explicit_shared_members?: boolean
	content_hash?: string
	file_lock_info?: string
}
interface IDeletedItem extends Metadata {}

export type IReadFolderEntry = IFile | IFolder | IDeletedItem

export interface IDownloadResult {
	client_modified: string
	content_hash: string
	fileBlob: {
		readonly size: number
		readonly type: string
		slice(start?: number, end?: number, contentType?: string): Blob
	}
	id: string
	is_downloadable: boolean
	name: string
	path_display: string
	path_lower: string
	rev: string
	server_modified: string
	size: number
}
