export const UPLOAD_FILE_SIZE_LIMIT = 150 * 1024 * 1024
export const MAX_BLOB = 8 * 1000 * 1000 // 8Mb - Dropbox JavaScript API suggested max file / chunk size
