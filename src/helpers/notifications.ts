import { notification } from 'antd'

import { IconType } from 'antd/es/notification/interface'

export const openNotificationWithIcon = (
	type: IconType,
	message: string,
	description: string
) => {
	notification[type]({
		message,
		description
	})
}
