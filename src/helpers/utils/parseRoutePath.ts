import { Params } from 'react-router-dom'

export const parseRoutePath = (props: Readonly<Params<string>>): string => {
	const splitedRoutes = Object.values(props).filter(el => !!el)
	if (!splitedRoutes.length) {
		return ''
	}
	return '/' + splitedRoutes.join('/')
}

