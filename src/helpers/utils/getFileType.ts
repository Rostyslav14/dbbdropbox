import { lookup } from 'mime-types'
import { uploadedItemType } from '../types/uploadedTypes'
export const getFileType = (arg: string): uploadedItemType => {
	let res = (lookup(arg) || 'text').split('/')[0] as uploadedItemType
	return res
}
