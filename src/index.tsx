import React from 'react'
import { createRoot } from 'react-dom/client'
import './styles/globals.scss'
import { RoutesList } from './routes/RoutesList'

const container = document.getElementById('root')!
const root = createRoot(container)

root.render(
	<React.StrictMode>
		<RoutesList />
	</React.StrictMode>
)
