import { IReadFolderEntry } from './../helpers/types/responseTypes'
import { Dropbox, DropboxResponse } from 'dropbox'
import { config } from '../config'
import { handleError } from './handlers'
import fileDownload from 'js-file-download'

const service = new Dropbox({
	accessToken: config.apiKey,
	fetch: window.fetch.bind(window)
})

export const dropboxService = {
	readFolder,
	deleteFile,
	downloadFile,
	downloadFolder,
	getFilePDFPreview,
	getFileThumbNail,
	createFolder,
	uploadFile,
	uploadFolder
}

export async function readFolder(
	folderPath: string
): Promise<IReadFolderEntry[]> {
	try {
		const response = await service.filesListFolder({ path: folderPath })
		return response.result.entries
	} catch (err) {
		throw handleError(err)
	}
}

export async function downloadFolder(folderPath: string): Promise<any> {
	try {
		const response: DropboxResponse<any> = await service.filesDownloadZip({
			path: folderPath
		})
		fileDownload(
			response.result.fileBlob,
			response.result.metadata.name + '.zip'
		)
		return response.result
	} catch (err) {
		throw handleError(err)
	}
}

export async function downloadFile(
	filePath: string
): Promise<Blob | MediaSource> {
	try {
		const response: DropboxResponse<any> = await service.filesDownload({
			path: filePath
		})
		fileDownload(response.result.fileBlob, response.result.name)
		return response.result.fileBlob
	} catch (err) {
		throw handleError(err)
	}
}
export async function getFilePDFPreview(filePath: string): Promise<Blob> {
	try {
		const response: DropboxResponse<any> = await service.filesGetPreview({
			path: filePath
		})
		return response.result.fileBlob
	} catch (err) {
		throw handleError(err)
	}
}
export async function getFileThumbNail(filePath: string): Promise<Blob> {
	try {
		const response: DropboxResponse<any> = await service.filesGetThumbnail({
			path: filePath,
			//@ts-ignore
			size: 'w2048h1536',
			//@ts-ignore

			mode: 'fitone_bestfit'
		})
		return response.result.fileBlob
	} catch (err) {
		throw handleError(err)
	}
}

export async function deleteFile(filePath: string): Promise<any> {
	try {
		const response = await service.filesDeleteV2({ path: filePath })
		return response.result
	} catch (err) {
		throw handleError(err)
	}
}

async function createFolder(path: string, folderName: string) {
	try {
		const response = await service.filesCreateFolderV2({
			autorename: false,
			path: path + '/' + folderName
		})
		return response.result
	} catch (err) {
		throw handleError(err)
	}
}

async function uploadFile(file: File, path: string) {
	try {
		const response = await service.filesUpload({
			path: path + '/' + file.name,
			contents: file
		})
		return response.result
	} catch (err) {
		throw handleError(err)
	}
}

async function uploadFolder(files: FileList, currentPath: string) {
	const results = []
	for (let i = 0; i < files.length; i++) {
		const file = files[i]
		const directories = file.webkitRelativePath.split('/')
		const path =
			currentPath + '/' + directories.slice(0, directories.length - 1).join('/') //get directories path
		results.push(uploadFile(file, path))
	}
	const res = await Promise.allSettled(results)
	return res
}
