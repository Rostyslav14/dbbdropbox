import { openNotificationWithIcon } from '../helpers/notifications'
export function handleError(error: any): Promise<Error> {
	if (!!error.message.length && !!error.name.length) {
		openNotificationWithIcon('error', error.name, error.message)
		return Promise.reject(error)
	}
	const errorBody = {
		message: 'Something went wrong :c',
		name: 'Server Error'
	}
	openNotificationWithIcon('error', errorBody.name, errorBody.message)

	return Promise.reject(errorBody)
}
