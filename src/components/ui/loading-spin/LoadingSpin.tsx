import { FC } from 'react'
import style from './loading-spin.module.scss'
interface Props {}
export const LoadingSpin: FC<Props> = props => {
	return <span className={style.loader}>Loading</span>
}
