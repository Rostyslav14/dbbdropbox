import { uploadedItemType } from '../../../helpers/types/uploadedTypes'
import { FC } from 'react'
import { Tag } from 'antd'
import { BsFileMusic, BsFileFont, BsCardImage } from 'react-icons/bs'
import { RiFolder3Fill } from 'react-icons/ri'
import { RxFile } from 'react-icons/rx'
import {
	AiOutlineFileText,
	AiOutlineDelete,
	AiOutlineVideoCamera,
	AiOutlineFolderOpen
} from 'react-icons/ai'
import style from './uploaded-file-icon.module.scss'
interface Props {
	type: uploadedItemType
}
export const UploadedFileIcon: FC<Props> = props => {
	const { type } = props
	let image
	let color
	switch (type) {
		case 'deleted':
			color = 'red'
			image = <AiOutlineDelete />
			break
		case 'folder':
			color = 'blue'
			image = <AiOutlineFolderOpen />
			break
		case 'audio':
			color = 'maroon'
			image = <BsFileMusic />
			break
		case 'model':
			color = 'pink'
			image = <RiFolder3Fill />
			break
		case 'font':
			color = 'olive'
			image = <BsFileFont />
			break
		case 'text':
			color = 'green'
			image = <AiOutlineFileText />
			break
		case 'video':
			color = 'violet'
			image = <AiOutlineVideoCamera />
			break
		case 'image':
			color = 'teal'
			image = <BsCardImage />
			break
		default:
			color = 'geekblue'
			image = <RxFile />
			break
	}

	return (
		<Tag className={style.tag} color={color} key={type}>
			{image}
		</Tag>
	)
}
