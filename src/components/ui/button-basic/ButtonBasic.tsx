import {
	ButtonHTMLAttributes,
	DetailedHTMLProps,
	FC,
	ReactNode
} from 'react'
import style from './button-basic.module.scss'

interface Props
	extends DetailedHTMLProps<
		ButtonHTMLAttributes<HTMLButtonElement>,
		HTMLButtonElement
	> {
	color: string
	children: ReactNode
}
export const ButtonBasic: FC<Props> = props => {
	const { color, children, ...rest } = props
	return (
		<button className={style.button} style={{ backgroundColor: color }} {...rest}>
			{children}
		</button>
	)
}
