import { FC } from 'react'
import { Select } from 'antd'
import { RiFileUploadLine } from 'react-icons/ri'
import { RiFolderUploadLine } from 'react-icons/ri'
import style from './select-basic.module.scss'

interface Props {
	onUpload: (file: File) => void
	onUploadFolder: (files: FileList) => void
}
export const SelectBasic: FC<Props> = props => {
	const { onUpload, onUploadFolder } = props
	return (
		<Select value='+Add New' className={'fileSelect'}>
			<Select.Option value='file'>
				<label className={style.uploadBtn}>
					<RiFileUploadLine />
					File
					<input
						className={style.fileInput}
						title='File'
						type='file'
						onChange={e => onUpload(e.target.files![0])}
					></input>
				</label>
			</Select.Option>
			<Select.Option value='folder'>
				<label className={style.uploadBtn}>
					<RiFolderUploadLine />
					Folder
					<input
						className={style.fileInput}
						title='Folder'
						type='file'
						//@ts-expect-error
						multiple=''
						webkitdirectory=''
						mozdirectory=''
						directory=''
						onChange={e => onUploadFolder(e.target.files!)}
					></input>
				</label>
			</Select.Option>
		</Select>
	)
}
