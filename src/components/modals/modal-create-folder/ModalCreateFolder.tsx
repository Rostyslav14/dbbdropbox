import { Button, Form, Input, Modal } from 'antd'
import { FC, FormEvent } from 'react'

const validationRules = {
	required: { required: true, message: 'Please input folder name!' },
	foldername: {
		pattern:
			/^[^\s^\x00-\x1f\\?*:"";<>|\/.][^\x00-\x1f\\?*:"";<>|\/]*[^\s^\x00-\x1f\\?*:"";<>|\/.]+$/,
		message: 'Incorrect folder name!'
	}
}

interface Props {
	isOpen: boolean
	onToggleModal: () => void
	onCreateFolder: (name: string) => void
}
export const ModalCreateFolder: FC<Props> = props => {
	const { isOpen, onToggleModal, onCreateFolder } = props
	const [form] = Form.useForm()

	return (
		<>
			<Button type='default' onClick={() => onToggleModal()}>
				Create Folder
			</Button>
			<Modal
				bodyStyle={{ height: 100 }}
				title='Write a folder name'
				centered
				open={isOpen}
				onOk={() => onToggleModal()}
				onCancel={() => onToggleModal()}
				footer={null}
			>
				<Form form={form} onFinish={e => onCreateFolder(e.foldername)}>
					<Form.Item
						name='foldername'
						rules={[validationRules.required, validationRules.foldername]}
					>
						<Input />
					</Form.Item>
					<Form.Item wrapperCol={{ offset: 20, span: 24 }}>
						<Button type='primary' htmlType='submit'>
							Submit
						</Button>
					</Form.Item>
				</Form>
			</Modal>
		</>
	)
}
