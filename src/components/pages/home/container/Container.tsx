import { FC, ReactNode } from 'react'
import style from './container.module.scss'
interface Props {
	children: ReactNode
}
export const Container: FC<Props> = props => {
	const { children } = props
	return (
		<div className={style.container}>
			<div className={style.contentWrapper}>{children}</div>
		</div>
	)
}
