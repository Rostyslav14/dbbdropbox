import { FC, ReactNode } from 'react'
import style from './toolbar.module.scss'

interface Props {
	children: ReactNode
}
export const Toolbar: FC<Props> = props => {
	const { children } = props
	return (
		<div className={style.container}>
			<h1 className={style.header}>Your Files</h1>
			<div className={style.buttonsWrapper}>{children}</div>
		</div>
	)
}
