import { Space } from 'antd'
import { ColumnsType } from 'antd/es/table'
import { ButtonBasic } from 'src/components/ui/button-basic/ButtonBasic'
import { UploadedFileIcon } from 'src/components/ui/uploaded-file-icon/UploadedFileIcon'
import { IReadFolderEntry } from 'src/helpers/types/responseTypes'
import { getFileType } from 'src/helpers/utils/getFileType'
import { parseFileSize } from 'src/helpers/utils/parseFileSize'

export const tableColumns = (
	onDownload: (row: IReadFolderEntry) => any,
	onDelete: (row: IReadFolderEntry) => any,
	onOpen: (row: IReadFolderEntry) => any
): ColumnsType<IReadFolderEntry> => [
	{
		title: 'Type',
		key: '.tag',
		dataIndex: '.tag',
		//here is i check if we need to make additional parse of file format (it's necessary if '.tag' property === 'file')
		// if '.tag' === file we took the file name and parse file type from item name
		render: function (_, row: IReadFolderEntry) {
			if (row['.tag'] === 'file') {
				return (
					<div onClick={() => onOpen(row)}>
						<UploadedFileIcon type={getFileType(row.name)} />
					</div>
				)
			}
			return (
				<div onClick={() => onOpen(row)}>
					<UploadedFileIcon type={row['.tag']} />
				</div>
			)
		}
	},
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		render: text => <p>{text || '--'}</p>
	},
	{
		title: 'Shared Users',
		dataIndex: 'sharedUsers',
		key: 'sharedUsers',
		render: text => <p>{text || '--'}</p>
	},
	{
		title: 'File Size',
		dataIndex: 'size',
		key: 'size',
		render: size => <p>{!!size ? parseFileSize(size) : '--'}</p>
	},
	{
		title: 'Modified',
		dataIndex: 'client_modified',
		key: 'client_modified',
		render: data => {
			if (data) {
				return <p>{new Date(data).toISOString().split('T')[0]}</p>
			}
			return '--'
		}
	},

	{
		title: 'Action',
		key: 'action',
		render: (_, record) => (
			<Space direction='vertical' size='middle'>
				<ButtonBasic onClick={() => onDownload(record)} color='#88B04B'>
					Download
				</ButtonBasic>
				<ButtonBasic onClick={() => onDelete(record)} color='#F7CAC9'>
					Delete
				</ButtonBasic>
			</Space>
		)
	}
]
