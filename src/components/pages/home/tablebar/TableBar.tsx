import { FC } from 'react'
import { Table } from 'antd'
import { tableColumns } from './tableColumns/tableColumns'
import { IReadFolderEntry } from 'src/helpers/types/responseTypes'

interface Props {
	data: IReadFolderEntry[]
	loading: boolean
	onDeleteItem: (row: IReadFolderEntry) => void
	onDownloadItem: (row: IReadFolderEntry) => void
	onOpen: (row: IReadFolderEntry) => void
}

export const TableBar: FC<Props> = props => {
	const { loading, data, onDeleteItem, onDownloadItem, onOpen } = props
	return (
		<div style={{ minHeight: '400px', maxHeight: '480px' }}>
			<Table
				scroll={{ y: 420 }}
				pagination={false}
				sticky={true}
				style={{ flex: 1 }}
				loading={loading}
				columns={tableColumns(onDownloadItem, onDeleteItem, onOpen)}
				dataSource={data}
			/>
		</div>
	)
}
