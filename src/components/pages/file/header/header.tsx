import { FC } from 'react'
import style from './header.module.scss'
import { Link } from 'react-router-dom'
import { IoIosArrowBack } from 'react-icons/io'
import { AiOutlineQuestionCircle } from 'react-icons/ai'
import { ButtonBasic } from 'src/components/ui/button-basic/ButtonBasic'
interface Props {
	onDownload: () => void
	onMutedButton: () => void
	path: string
}
export const Header: FC<Props> = props => {
	const { onDownload, onMutedButton, path } = props
	return (
		<header className={style.header}>
			<div className={style.container}>
				<div className={style.leftSideWrapper}>
					<Link className={style.iconWrapper} to={'/'}>
						<IoIosArrowBack size={25} />
					</Link>
					<h1 className={style.title}>{path}</h1>
					<div className={style.buttonsContainer}>
						<ButtonBasic onClick={() => onDownload()} color='#88B04B'>
							Download
						</ButtonBasic>
						<ButtonBasic onClick={() => onMutedButton()} color='#576CBC'>
							Change
						</ButtonBasic>
					</div>
				</div>
				<div className={style.rightSideWrapper}>
					<ButtonBasic onClick={() => onMutedButton()} color='#576CBC'>
						Share
					</ButtonBasic>

					<div onClick={() => onMutedButton()} className={style.iconWrapper}>
						<AiOutlineQuestionCircle size={25} />
					</div>
				</div>
			</div>
		</header>
	)
}
