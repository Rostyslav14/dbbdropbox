import { FC } from 'react'
import { Link } from 'react-router-dom'
import style from './error-plug.module.scss'
interface Props {}
export const ErrorPlug: FC<Props> = props => {
	return (
		<div className={style.container}>
			<h1>At the moment, files of this format cannot be opened for preview.</h1>
			<Link className={style.button} to={'/'}>
				Home
			</Link>
		</div>
	)
}
